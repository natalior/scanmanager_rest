from flask_restful import Resource, Api
from flask import Flask, request, Response, jsonify
import json
import subprocess
import base


app = Flask(__name__)
api = Api(app)


IP_ADDRESS = '0.0.0.0'
PORT = 5002


def get_error_response(message, status):
    resp_message = {
        'message': message,
        'status': status
    }
    resp = jsonify(resp_message)
    resp.status_code = status
    return resp


class Scan(Resource):
    def get(self, scan_id):
        try:
            output = base.run_cmd(["udmtools pm -n psh -S", scan_id, '--json'])
            resp = Response(output, status=200, mimetype='application/json')
            return resp
        except subprocess.CalledProcessError as e:
            message = "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output.strip())
            status = 400
            return get_error_response(message, status)

    def delete(self, scan_id):
        try:
            output = base.run_cmd(["udmtools smc -K", scan_id, "--force", "--json"])
            resp = Response(output, status=200, mimetype='application/json')
            return resp
        except subprocess.CalledProcessError as e:
            message = "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output.strip())
            status = 400
            return get_error_response(message, status)



class Scans(Resource):
    def get(self):
        return "all scans"

    def post(self):
        data = request.json
        input = data['input']
        scan_group = data['scan_group']
        try:
            output = base.run_cmd(["udmtools ss --trigger -S", scan_group, "-a", input, "--json"])
            js = output #CHECK!!!!
            resp = Response(js, status=200, mimetype='application/json')
            return resp
        except subprocess.CalledProcessError as e:
            message = "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output.strip())
            status = 400
            return get_error_response(message, status)




api.add_resource(Scan, '/scan/<scan_id>') # Route_1
api.add_resource(Scans, '/scans') # Route_2


if __name__ == '__main__':
     app.run(host=IP_ADDRESS, port=PORT, debug=True)