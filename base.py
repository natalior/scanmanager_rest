import subprocess
import sys


def run_cmd(cmd):
    cmd_str = cmd if isinstance(cmd, str) else " ".join(cmd)
    streamdata = subprocess.check_output(cmd_str, stderr=subprocess.STDOUT, shell=True,
                                         universal_newlines=True)
    return streamdata